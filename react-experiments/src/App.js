import React, { Component } from 'react';
import Item from './Item';
import DataFetcher from './DataFetcher';
import Users from './Users';

import data from './data';

class App extends Component {

  state = {
    data,
    users: [],
  }

  componentWillMount() {
    this.ts = Date.now();
  }

  componentDidMount() {
    this.dataFetcher = new DataFetcher();
    this.dataFetcher.onChange(this.updateData);
    this.setState({ users: this.dataFetcher.getAll() });
  }

  componentDidUpdate() {
    console.log(`Updated: ${Date.now() - this.ts}`);
  }

  updateData = (users) => {
    // console.log(JSON.stringify(users));
    this.ts = Date.now();
    this.setState({ users });
  }

  handleCheckbox = curr => {
    console.log(curr);
  }
  
  render() {
    return (
      <div className="App">
        {/* <aside>
          <ul className="currList">
            {this.state.data.map(curr => ( <Item key={curr.id} onClick={this.handleCheckbox} item={curr} /> ))}
          </ul>
        </aside> */}
        <Users users={this.state.users} />
      </div>
    );
  }
}

export default App;

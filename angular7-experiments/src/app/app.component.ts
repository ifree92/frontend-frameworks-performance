import { AfterViewChecked, AfterViewInit, Component, DoCheck, OnChanges, SimpleChanges } from '@angular/core';
import data from '../data';

export interface IUserItem {
  id: number;
  name: string;
  age: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck, OnChanges, AfterViewInit, AfterViewChecked {
  public data = data;
  public users: IUserItem[] = [];
  private counter = 0;
  private ts = 0;

  constructor() {
    const users = [];
    for (let i = 0; i < 4000; i++) {
      users[i] = { id: i + 1, name: `hello-${i + 1}`, age: i + 1 };
    }
    this.users = users;
    setInterval(this.interval, 100);
  }

  private interval = () => {
    this.users[this.counter++ % this.users.length].age = this.counter * 2;
    this.ts = Date.now();
  }

  public onChange(item) {
    console.log(item);
  }

  ngDoCheck(): void {
    // console.log('ngDoCheck');
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log('ngOnChanges');
  }

  ngAfterViewInit(): void {
    // console.log('ngAfterViewInit');
  }

  ngAfterViewChecked(): void {
    // console.log('ngAfterViewChecked', Date.now() - this.ts);
  }
}

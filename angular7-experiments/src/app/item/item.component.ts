import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent {

  @Input() public item;
  @Output() someevent: EventEmitter<any> = new EventEmitter();

  onClick() {
    this.someevent.emit(this.item);
  }
}

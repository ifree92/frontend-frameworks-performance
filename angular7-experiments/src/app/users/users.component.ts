import { Component, Input } from '@angular/core';
import { IUserItem } from '../app.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent  {
  @Input() users: IUserItem[] = [];
}

export default class DataFetcher {
  data = [];
  counter = 0;
  CBS_ON_CHANGE = [];

  constructor() {
    for (let i = 0; i < 4000; i++) {
      this.data[i] = { id: i + 1, user: `hello-${i + 1}`, age: i + 1 };
    }
    setInterval(this.interval, 100);
  }

  interval = () => {
    this.data[this.counter++ % this.data.length].age = this.counter * 2;
    this.emitOnChange();
  }

  getAll() {
    return this.data;
  }

  emitOnChange() {
    this.CBS_ON_CHANGE.forEach(cb => cb(this.data));
  }

  onChange(cb) {
    this.CBS_ON_CHANGE.push(cb);
  }
}
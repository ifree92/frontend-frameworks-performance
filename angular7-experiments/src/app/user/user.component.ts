import { Component, Input, OnInit } from '@angular/core';
import { IUserItem } from '../app.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  @Input() user: IUserItem;
}

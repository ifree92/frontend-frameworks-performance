import React, { Component } from 'react';

class Item extends Component {

  onClick = () => {
    this.props.onClick(this.props.item);
  }

  render() {

    const { item } = this.props;

    return (
      <li className="currItem">
        <input type="checkbox" id={item.id} onChange={this.onClick} />
        <label htmlFor={item.id}>{item.text.toUpperCase()}</label>
      </li>
    );
  }
}

export default Item;

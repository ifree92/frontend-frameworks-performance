import React, { PureComponent, Component } from 'react';

class UserItem extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.age !== this.props.age;
  }


  render() {
    const { user, age } = this.props;

    return (
      <li>
        <b>Name: </b>
        <span>{user}</span>
        <b>Age: </b>
        <span>{age}</span>
      </li>
    )
  }
}

export default UserItem;

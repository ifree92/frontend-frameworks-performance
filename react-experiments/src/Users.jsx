import React, { Component } from 'react';
import UserItem from './UserItem';

class Users extends Component {
  render() {
    return (
      <ul>
        {this.props.users.map(user => (
          <UserItem key={user.id} name={user.user} age={user.age} />
        ))}
      </ul>
    )
  }
}

export default Users;
